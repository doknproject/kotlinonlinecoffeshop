package com.example.myapplication
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.brush2
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.mycolor14
import com.example.myapplication.ui.theme.mycolor15
import com.example.myapplication.ui.theme.mycolor16
import com.example.myapplication.ui.theme.mycolor17
import com.example.myapplication.ui.theme.mycolor18
import com.example.myapplication.ui.theme.mycolor19
import com.example.myapplication.ui.theme.mycolor20
import com.example.myapplication.ui.theme.mycolor21
import com.example.myapplication.ui.theme.mycolor22
import com.example.myapplication.ui.theme.mycolor24

@Composable
fun MenuDetail (navController: NavController) {
    Box(
        Modifier
            .fillMaxSize()
            .background(mycolor22)){
        Column(Modifier.fillMaxSize()) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(30.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                ) {
                var controller by remember {
                    mutableStateOf(false)
                }
                Icon(painter = painterResource(id = R.drawable.arrow_left)
                    , contentDescription = null ,
                    tint = mycolor14,
                    modifier = Modifier
                        .size(24.dp)
                        .clickable { navController.popBackStack() }
                )
                Text(
                    text = "Detail",
                    color = mycolor14,
                    fontSize = 18.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold
                )
                Icon(painter = painterResource(id = R.drawable.heart2)
                    , contentDescription = null,
                    tint = if (!controller) mycolor14 else Color.Red,
                    modifier = Modifier
                        .size(24.dp)
                        .clickable { controller = !controller })


            }
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Box(
                    Modifier
                        .width(350.dp)
                        .height(235.dp)
                        .clip(
                            RoundedCornerShape(16.dp)
                        )
                )
                {
                    androidx.compose.foundation.Image(
                        painter = painterResource(id = R.drawable.coffepic2),
                        contentDescription = null,
                        Modifier
                            .width(350.dp)
                            .height(235.dp),
                        contentScale = ContentScale.Crop,

                        )
                }
            }
                Text(
                    modifier = Modifier.padding( start = 30.dp, top = 10.dp ),
                    text = "Cappucino",
                    color = mycolor14,
                    fontSize = 20.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold
                    )
            // start

            Box(
                Modifier
                    .padding(start = 30.dp, top = 4.dp)
                    .fillMaxWidth(),

                )
            {

                Column(Modifier.fillMaxWidth(),
                    ) {
                    Text(
                        text = " with Chocolate ",
                        color = mycolor15,
                        fontSize = 12.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.Normal
                        )
                    Spacer(modifier = Modifier.height(18.dp))
                    Row(Modifier.fillMaxWidth()){

                        Icon(painter = painterResource(id = R.drawable.star)
                            , contentDescription = null ,
                            tint = mycolor16,
                            modifier = Modifier
                                .size(20.dp)
                        )
                        Text(
                            modifier = Modifier
                                .offset(y = (-5).dp),
                            text = "4.8",
                            color = mycolor14,
                            fontSize = 16.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.SemiBold

                        )
                        Text(
                            text = "(230)",
                            color = mycolor17,
                            fontSize = 12.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.Normal

                        )



                    }
                }

                Box (
                    Modifier
                        .padding(start = 230.dp, top = 5.dp)
                        .size(44.dp)
                        .clip(RoundedCornerShape(14.dp))
                        .background(mycolor18),
                    Alignment.Center
                ) {

                    Icon(
                        painter = painterResource(id = R.drawable.bean),
                        contentDescription = null ,
                        tint = myColor1,
                        modifier = Modifier
                            .size(24.dp)

                    )

                }
                Box (
                    Modifier
                        .padding(start = 290.dp, top = 5.dp)
                        .size(44.dp)
                        .clip(RoundedCornerShape(14.dp))
                        .background(mycolor18),
                    Alignment.Center
                ) {

                    Icon(
                        painter = painterResource(id = R.drawable.milk),
                        contentDescription = null ,
                        tint = myColor1,
                        modifier = Modifier
                            .size(24.dp)

                    )

                }


            }

            //start divider
            Spacer(modifier = Modifier.height(10.dp))

            Divider(
                color = mycolor19,
                thickness = 1.dp,
                modifier = Modifier
                    .width(385.dp)
                    .padding(start = 30.dp)
            )
            Spacer(modifier = Modifier.height(10.dp))

            Text("Description",
                modifier = Modifier.padding(start = 30.dp),
                color = mycolor14,
                fontSize = 16.sp,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold)

            Spacer(modifier = Modifier.height(10.dp))

            Text(text = "A cappuccino is an approximately 150 m",
                modifier = Modifier
                    .padding(start = 30.dp),
                color = mycolor20,
                fontFamily = Sora,
                fontWeight = FontWeight.Normal,
                fontSize = 14.sp
            )
            Text(text = "l coffe (5oz) verage, with 25 ml of espres ",
                    modifier = Modifier
                    .padding(start = 30.dp),
                color = mycolor20,
                fontFamily = Sora,
                fontWeight = FontWeight.Normal,
                fontSize = 14.sp
            )

            Row(
                modifier = Modifier
                    .padding(start =30.dp)

            ) {
                Text(text = "so and 85ml of fresh..",
                    color = mycolor20,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    fontSize = 14.sp)
                Text(text = "Read More",
                    color = myColor1,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 14.sp,
                    modifier = Modifier
                        .clickable {  }
                    )

            }
            Spacer(modifier = Modifier.height(10.dp))

            Text("Size",
                modifier = Modifier.padding(start = 30.dp),
                color = mycolor14,
                fontSize = 16.sp,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold)
            Spacer(modifier = Modifier.height(10.dp))
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 30.dp, end = 23.dp),
                horizontalArrangement = Arrangement.SpaceBetween
                ) {
                var selected by remember {
                    mutableStateOf(2)
                }
                Box(
                    modifier = Modifier
                        .width(96.dp)
                        .height(43.dp)
                        .clip(RoundedCornerShape(12.dp))
                        .background(if (selected == 1) mycolor24 else Color.Transparent)
                        .border(
                            1.dp, if (selected == 1) brush2 else
                                mycolor21, RoundedCornerShape(12.dp)
                        )
                        .clickable { selected = 1 },
                            contentAlignment = Alignment.Center
                ) {

                        
                        Text(text = "S",
                            fontSize = 14.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.Normal,
                            color = mycolor14

                            )



                }
                Box(
                    modifier = Modifier
                        .width(96.dp)
                        .height(43.dp)
                        .clip(RoundedCornerShape(12.dp))
                        .background(if (selected == 2) mycolor24 else Color.Transparent)
                        .border(
                            1.dp, if (selected == 2) brush2 else
                                mycolor21, RoundedCornerShape(12.dp)
                        )
                        .clickable { selected = 2 },
                    contentAlignment = Alignment.Center
                ) {


                        Text(text = "M",
                            fontSize = 14.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.Normal,
                            color = mycolor14
                        )

                }
                Box(
                    modifier = Modifier
                        .width(96.dp)
                        .height(43.dp)
                        .clip(RoundedCornerShape(12.dp))
                        .background(if (selected == 3) mycolor24 else Color.Transparent)
                        .border(
                            1.dp, if (selected == 3) brush2 else
                                mycolor21, RoundedCornerShape(12.dp)
                        )
                        .clickable { selected = 3 },
                    contentAlignment = Alignment.Center
                ) {


                        Text(text = "L",
                            fontSize = 14.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.Normal,
                            color = mycolor14


                        )



                }

            }

        }
    }

}

