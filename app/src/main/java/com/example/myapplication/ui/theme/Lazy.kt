package com.example.myapplication.ui.theme

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import org.intellij.lang.annotations.Language
/*val language = listOf(
    "java",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin",
    "kotlin"
)

@Composable
fun myApp(modifier: Modifier = Modifier, languages: List<String>) {
    Column(modifier.fillMaxSize()) {
        LazyRow {
            items(languages) { item ->
                RowItem(item)
            }
        }
    }
}
@Composable
fun CoulumnItem(modifier: Modifier = Modifier, name : String) {



    
}@Composable
fun RowItem(modifier: Modifier = Modifier, name : String) {
}
*/
@Composable
fun MyLazyRow() {
    val cardData = listOf(
        CardData("Card 1", "Description 1"),
        CardData("Card 2", "Description 2"),
        CardData("Card 3", "Description 3"),
        CardData("Card 4", "Description 4"),
        CardData("Card 5", "Description 5"),
        CardData("Card 6", "Description 6"),
        CardData("Card 7", "Description 7"),
        CardData("Card 8", "Description 8")
    )

    LazyRow {
        items(cardData) { card ->
            CardItem(card)
        }
    }
}

data class CardData(val title: String, val description: String)

@Composable
fun CardItem(cardData: CardData) {
    Card(
        modifier = Modifier
            .width(200.dp)
            .padding(16.dp)
            .clickable {  }

    ) {
        Column(
            modifier = Modifier.padding(16.dp)

        ) {

            Text(text = cardData.title, fontFamily = Sora)
            Text(text = cardData.description, fontFamily = Sora)

        }
    }
}