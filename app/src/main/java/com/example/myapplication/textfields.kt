package com.example.myapplication

import android.provider.ContactsContract.CommonDataKinds.Phone
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material.icons.outlined.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp

//in zarde ro avordim ta keyboardcontriller moteghayeresh ro besazim
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TextFieldComponent() {
    // in line ro daghigh nemidonam  ????????????????????????????
    val context = LocalContext.current
    var textNewValue by remember {
        mutableStateOf("")
    }
    // baraye action dokme send estefade shodesh
    val KeyboardController = LocalSoftwareKeyboardController.current
    val FocusManager = LocalFocusManager.current
    var textNewValue2 by remember {
        mutableStateOf("")
    }
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(verticalArrangement = Arrangement.spacedBy(25.dp)) {
            TextField(

                colors = TextFieldDefaults.colors(
                    // khate zire textfield mire ba in do khat hala
                    //dar halat focus ya unfocus
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedContainerColor = Color.Transparent,

                ),
                value = textNewValue, onValueChange = { newText ->
                textNewValue = newText
            },
                //elam mikone ke masalan search coffe has
                label = { Text(text = "Enter your E-Mail") },
                singleLine = true,
                // age ziad char vared konim bozorg nemishe text fieldemon
                modifier = Modifier.width(280.dp),
                placeholder = {
                    // in be ma hint mide ke ba che formati bayad vared konim.
                    Text(text = "abc@gmail.com")
                },
                // vaghti karbar dare vared mikone setare mizare ke dide nashe.
               // visualTransformation = PasswordVisualTransformation(),
                leadingIcon = {
                    // iconie ke samte chap miad va namad ine ke chikar mikone
                    Icon(imageVector = Icons.Outlined.Search,
                        contentDescription = "search icon" ,
                    //    tint = Color.Black
                    )
                },
                trailingIcon = {
                    //
                    IconButton(onClick = {
                        // chap mikone ke chi neveshti to text
                        Toast.makeText(context, textNewValue, Toast.LENGTH_SHORT).show()
                    }) {
                        // iconie ke rosh click mikoni bokoni va ye action etefagh miofte
                       // box abi mindaze dor on icon
                        Box(modifier = Modifier.background(color = Color.Blue)) {
                            // mitonim in ro betor export ham begirim va estefade konim
                            Icon(
                                painter = painterResource(id = R.drawable.furnitur_icon),
                                contentDescription = " send icon ",
                                //rang icon samt rast ghermez
                               // tint = Color.Red
                            )

                        }

                        }

                },
                keyboardOptions = KeyboardOptions(

                    // horof bozorg dar keyboard
                   // capitalization = KeyboardCapitalization.Characters
                    // avalin harf bozorg baghie kochik
                    // capitalization = KeyboardCapitalization.Sentences
                    // number :
                    keyboardType = KeyboardType.Phone,
                    // dokme send  keyboard ro thaghir mide
                    imeAction = ImeAction.Send
                ),
                keyboardActions = KeyboardActions(
                    // ba zadan dokme send keyboard in etefaghat miofte
                    onSend = {
                        // bad az zadan dokme send hide mishe dokme send
                        KeyboardController?.hide()
                        //ye toast message baraye bad az zadan dokme send
                        Toast.makeText(context, "you clicked it", Toast.LENGTH_SHORT).show()
                        FocusManager.clearFocus()
                    },


                ),
                shape = RoundedCornerShape(25.dp)

            )
        }
    }

    // how to remove the line from the text field
}
