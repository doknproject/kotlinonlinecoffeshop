package com.example.myapplication

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import com.example.myapplication.ui.theme.Sora

@Composable
fun heart(modifier: Modifier = Modifier) {

    Column(Modifier.fillMaxSize(),
    )
        {
        Box(Modifier.fillMaxSize()
            .background(Color.Red),
            contentAlignment = Alignment.Center,
            )
        {
            Text(
                text = "Heart",
                color = Color.Black,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold,
            )
        }
        }

}
//2

@Composable
fun bag(modifier: Modifier = Modifier) {

    Column(Modifier.fillMaxSize(),
    )
    {
        Box(Modifier.fillMaxSize()
            .background(Color.Blue),
            contentAlignment = Alignment.Center,
        )
        {
            Text(
                text = "bag",
                color = Color.Black,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold,
            )
        }
    }

}

@Composable
fun notification(modifier: Modifier = Modifier) {

    Column(Modifier.fillMaxSize(),
    )
    {
        Box(Modifier.fillMaxSize()
            .background(Color.Yellow),
            contentAlignment = Alignment.Center,
        )
        {
            Text(
                text = "Notification",
                color = Color.Black,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold,
            )
        }
    }

}