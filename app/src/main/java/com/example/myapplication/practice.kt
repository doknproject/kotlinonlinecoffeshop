package com.example.myapplication

import android.text.style.BackgroundColorSpan
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import androidx.compose.ui.unit.sp
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.myColor10
import com.example.myapplication.ui.theme.myColor11

// Function to define a coffee box with a specific name
/*
val languages = listOf(
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
    "java",
)


@Composable
fun myapp(modifier: Modifier = Modifier, languages: List<String>){

}

@Composable
fun ColumnItem(modifier: Modifier , name : String) {
    
}

@Composable
fun RowItem(modifier: Modifier , name : String) {
}*/

/*
@Composable
fun MyScreen(cardsData: List<Map<String, Any>>) {
    LazyVerticalGrid(columns = GridCells.Fixed(2)) {
        items(cardsData) { map ->
            myCard(map)
        }
    }
}

@Composable
fun MyGrid() {
    val cardsData: List<Map<String, Any>> = listOf(
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Oat Milk",
            "price" to "$ 3.90"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Oat Milk",
            "price" to "$ 3.90"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53"
        ),
    )

    MyScreen(cardsData = cardsData)
}

@Composable
fun  myCard(map: Map<String, Any>, modifier: Modifier = Modifier) {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Card(
            modifier = Modifier
                .height(239.dp)
                .width(149.dp),
            elevation = CardDefaults.cardElevation(10.dp),
            //border = BorderStroke(3.dp, Color.Gray),
            colors = CardDefaults.cardColors(Color.White)
        ) {
            Column {
                Box( contentAlignment = Alignment.Center, modifier = Modifier
                    .width(141.dp)
                    .height(132.dp)
                    .clip(RoundedCornerShape(16.dp))
                    .padding(start = 8.dp, top = 8.dp)

                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(id = map["image"] as Int),
                        contentDescription = null,
                        modifier = Modifier
                            .width(141.dp)
                            .height(132.dp)
                            .clip(RoundedCornerShape(16.dp)),
                        contentScale = ContentScale.Crop
                    )

                }
                Text(text = "Cappocino",
                color = Color.Black,
                fontFamily = Sora,
                fontSize = 16.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier
                    .padding(start = 14.dp, top = 11.dp)

                )

                Text(text = map["content"] as String,
                    color = myColor10,
                    fontFamily = Sora,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Normal,
                    modifier = Modifier
                        .padding(start = 11.dp)

                )

                Row(
                    modifier = Modifier
                    .fillMaxWidth()
                        .padding(start = 14.dp , end = 5.dp, top = 10.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = map["price"] as String,
                        color = myColor11,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 18.sp,
                        modifier = Modifier
                          //  .padding(start = 14.dp, top = 12.dp)
                      )
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .size(32.dp)
                          //  .padding(start = 5.dp)
                            //.padding.dp)
                            .clip(RoundedCornerShape(10.dp))
                         //  .padding(start = (14.dp))
                            .background(myColor1)

                    )
                    {
                        Text(text = "+",
                            color = Color.White)

                    }

                }

            }


        }
    }

}
*/
/*
@Composable
fun CardItem(title: String, description: String) {
    Box(
        modifier = Modifier
            .padding(16.dp)
            .background(Color.White)
            .clip(RoundedCornerShape(12.dp))
            .height(200.dp)
            .width(150.dp)
            .clickable { /* Handle card click event */ }
    ) {
        Column(Modifier.padding(16.dp)) {
            Text(
                text = title,
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                color = Color.Black
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = description,
                fontSize = 14.sp,
                color = Color.Gray,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}
@Composable
fun CardList(cardItems: List<Pair<String, String>>) {
    LazyRow {
        items(cardItems) { cardItem ->
            CardItem(title = cardItem.first, description = cardItem.second)
        }
    }
}
*/

