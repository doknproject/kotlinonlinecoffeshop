package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInHorizontally
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.ui.theme.MyApplicationTheme
import kotlinx.coroutines.delay


class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            var barTurn by remember {
                mutableStateOf(1)
            }
            var count = remember {
                mutableStateOf(1)
            }
            var controllerIcons = rememberNavController()
            val navState = rememberNavController()
            MyApplicationTheme {
                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    bottomBar = {
                        if (barTurn == 1) {
                            //   BottomBar1()
                        } else if (barTurn == 2) {
                            BottomBar1(navState)
                        } else if (barTurn == 3) {
                            BottomBar2(navState)
                        } else if (barTurn == 4) {
                            BottomBar3(navState, count)

                        }

                    }
                ) {

                    NavHost(navController = navState, startDestination = "homePage") {
                        composable(route = "homePage") {
                            EnterAnimation {
                                Homepage(navState)

                            }
                            barTurn = 1
                        }
                        composable(route = "menuPage") {
                            EnterAnimation {
                                Menu(navState)
                            }

                            barTurn = 2
                        }
                        composable(route = "detailPage") {
                            EnterAnimation {
                                MenuDetail(navState)
                            }

                            barTurn = 3
                        }
                        composable(route = "orderPage") {
                            EnterAnimation {
                                orderPage(navState, count)
                            }
                            barTurn = 4
                        }
                        composable(route = "lastPage") {
                            EnterAnimation {
                                lastPage(navState)
                            }
                            barTurn = 5
                        }
                        composable(route = "heart") {
                            EnterAnimation {
                                heart()
                            }
                            barTurn = 2
                        }
                        composable(route = "bag") {
                            EnterAnimation {

                                bag()
                            }
                            barTurn = 2
                        }
                        composable(route = "notification") {
                            EnterAnimation {
                                notification()
                            }
                            barTurn = 2
                        }
                        //ss

                    }
                }
            }
        }
    }
}


@Composable
fun EnterAnimation(content: @Composable () -> Unit) {
    var toggle by remember { mutableStateOf(false) }

    LaunchedEffect(Unit) {
        toggle = true
        delay(1000) // Wait for 1 second (adjust the duration as needed)
    }

    AnimatedVisibility(
        visible = toggle,
        enter = slideInHorizontally(
            initialOffsetX = { if (toggle) it else -it }
        ),
    ) {
        content()
    }
}