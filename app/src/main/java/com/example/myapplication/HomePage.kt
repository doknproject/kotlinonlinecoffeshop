package com.example.myapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.myColor1

//Homepage
@Composable
fun Homepage(navController: NavController) {

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)

    ) {
        Image(
            painter = painterResource(id = R.drawable.image1),
            contentDescription = "image1",
            modifier = Modifier
                .requiredHeight(550.dp)
                .requiredWidth(550.dp)
                .offset(y = (-55).dp)


        )

        Box(
            modifier = Modifier.align(Alignment.BottomCenter)
                //.fillMaxSize()
                //.background(Color.Black)
            ,
            contentAlignment = Alignment.BottomCenter
        ) {
            Column( modifier = Modifier.align(Alignment.TopCenter), horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = "Coffee so good,\nyour taste buds\nwill love it",color =Color.White,
                    textAlign = TextAlign.Center,
                    maxLines = 3,
                    fontSize = 34.sp,
                    fontFamily = Sora, fontWeight = FontWeight.SemiBold,
                    lineHeight = 40.sp,
                    modifier = Modifier
                        .offset(y =(-5).dp)
                )
                Text(
                    text = "The best grain, the finest roast, the\npowerful flavor",
                    color = Color(0xFFA9A9A9),
                    textAlign = TextAlign.Center,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    fontSize = 14.sp,
                    modifier = Modifier
                        .offset(y =(-5).dp)



                )
                Box(
                    contentAlignment = Alignment.Center,
                    //modifier = Modifier.fillMaxSize()
                ) {
                    Button(
                        onClick = { navController.navigate("menuPage") }, colors =
                        ButtonDefaults.buttonColors(
                            containerColor =  myColor1
                        ),
                        shape = RoundedCornerShape(25.dp)
                        ,
                        modifier = Modifier
                            .padding(16.dp)
                            .width(350.dp)
                            .height(65.dp)
                            .offset(y =(-5).dp)

                    ) {
                        Text(text = "Get Started", color = Color.White,
                            fontSize = 20.sp)
                    }
                }
            }
        }}}

