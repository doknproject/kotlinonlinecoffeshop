package com.example.myapplication

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.brush1
import com.example.myapplication.ui.theme.brush2
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.mycolor20
import com.example.myapplication.ui.theme.mycolor25
import com.example.myapplication.ui.theme.mycolor26
import com.example.myapplication.ui.theme.mycolor35

@Composable
fun BottomBar1(navController: NavController) {
    BottomAppBar(
        modifier = Modifier
            .fillMaxWidth()
            .height(99.dp)
            .clip(RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp))
        ,
        containerColor = Color.White
    ) {
        var controller by remember {
            mutableStateOf(1)
        }
        var brush = Brush.linearGradient(colors = listOf(brush1, brush2))
        var brush3 = Brush.linearGradient(colors = listOf(Color.White, Color.White))
        Row(
            Modifier.fillMaxSize(),
            Arrangement.SpaceEvenly,
            Alignment.CenterVertically
        ) {
            Column(
                verticalArrangement = Arrangement.Center, horizontalAlignment =
                Alignment.CenterHorizontally
            ) {
                IconButton(modifier = Modifier
                    .padding(0.dp), onClick = { controller = 1
                    navController.navigate("MenuPage")
                     }) {
                    Icon(

                        painter = painterResource(id = R.drawable.home),
                        modifier =
                        Modifier
                            .size(27.dp),
                        contentDescription = null,
                        tint = if (controller == 1) brush2 else Color(0xFF8d8d8d)
                    )
                }
                Box(
                    modifier = Modifier
                        .width(10.dp)
                        .height(5.dp)
                        .clip(RoundedCornerShape(18.dp))
                        .background(if (controller == 1) brush else brush3),


                    ) {

                }


            }

//2
            Column(
                verticalArrangement = Arrangement.Center, horizontalAlignment =
                Alignment.CenterHorizontally
            ) {
                IconButton(modifier = Modifier
                    .padding(0.dp), onClick = { controller = 2
                    navController.navigate("heart")}) {
                    Icon(

                        painter = painterResource(id = R.drawable.heart),
                        modifier =
                        Modifier
                            .size(27.dp),
                        contentDescription = null,
                        tint = if (controller == 2) brush2 else Color(0xFF8d8d8d)
                    )
                }
                Box(
                    modifier = Modifier
                        .width(10.dp)
                        .height(5.dp)
                        .clip(RoundedCornerShape(18.dp))
                        .background(if (controller == 2) brush else brush3),


                    ) {

                }
            }


//3
            Column(
                verticalArrangement = Arrangement.Center, horizontalAlignment =
                Alignment.CenterHorizontally
            ) {
                IconButton(modifier = Modifier
                    .padding(0.dp), onClick = { controller = 3
                    navController.navigate("bag")}
                ) {
                    Icon(

                        painter = painterResource(id = R.drawable.bag_2),
                        modifier =
                        Modifier
                            .size(27.dp),
                        contentDescription = null,
                        tint = if (controller == 3) brush2 else Color(0xFF8d8d8d)
                    )
                }
                Box(
                    modifier = Modifier
                        .width(10.dp)
                        .height(5.dp)
                        .clip(RoundedCornerShape(18.dp))
                        .background(if (controller == 3) brush else brush3),


                    ) {

                }


            }


//4
            Column(
                verticalArrangement = Arrangement.Center, horizontalAlignment =
                Alignment.CenterHorizontally
            ) {
                IconButton(modifier = Modifier
                    .padding(0.dp), onClick = { controller = 4
                    navController.navigate("notification")}) {
                    Icon(

                        painter = painterResource(id = R.drawable.notification),
                        modifier =
                        Modifier
                            .size(27.dp),
                        contentDescription = null,
                        tint = if (controller == 4) brush2 else Color(0xFF8d8d8d)
                    )
                }
                Box(
                    modifier = Modifier
                        .width(10.dp)
                        .height(5.dp)
                        .clip(RoundedCornerShape(18.dp))
                        .background(if (controller == 4) brush else brush3),

                    ) {
                }
            }
        }
    }

}

@Composable
fun BottomBar2(navController: NavController) {

    BottomAppBar(
        modifier = Modifier
            .height(90.dp)
            .clip(RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)),
        containerColor = Color.White

    ) {

        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly

        )
        {
            Column() {

                Text(
                    text = "Price",
                    color = mycolor20,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    fontSize = 14.sp,
                )
                Text(
                    text = "$ 4.53",
                    color = myColor1,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 18.sp,
                )

            }
            Spacer(modifier = Modifier.width(15.dp))
            Box(
                Modifier
                    .width(217.dp)
                    .height(67.dp)
                    .clip(RoundedCornerShape(16.dp))
                    .background(myColor1)
                    .clickable { navController.navigate("orderPage") },
                contentAlignment = Alignment.Center
            ) {

                Text(
                    text = "Buy Now",
                    color = Color.White,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 16.sp,
                )

            }


        }

    }

}

@Composable
fun BottomBar3(navController: NavController, count :MutableState<Int>) {

    BottomAppBar(
        modifier = Modifier
            .height(115.dp)
            .clip(RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)),
        containerColor = Color.White

    ) {
        Column(Modifier.fillMaxSize()) {

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp, end = 33.dp, top = 5.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically

                ) {

                Row(
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,

                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.moneys),
                        contentDescription = null,
                        modifier = Modifier
                            .size(30.dp),
                        tint = myColor1
                    )
                    Spacer(modifier = Modifier.width(5.dp))
                    Box(
                        Modifier

                            .clip(RoundedCornerShape(14.dp))
                            .background(mycolor26)
                    )
                    {
                        Row(
                            Modifier
                                .width(120.dp)
                                .height(30.dp)
                        ) {


                            Box(
                                modifier = Modifier
                                    .width(51.dp)
                                    .fillMaxHeight()
                                    .clip(RoundedCornerShape(20.dp))
                                    .background(myColor1),
                                Alignment.Center

                            ) {
                                    Text(
                                        text = "Cash",
                                        color = Color.White,
                                        fontSize = 12.sp,
                                        fontFamily = Sora,
                                        fontWeight = FontWeight.Normal,
                                        textAlign = TextAlign.Center
                                    )

                            }

                            // box 2
                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .clip(RoundedCornerShape(10.dp))
                                    .background(Color.Transparent),
                                Alignment.Center


                            ) {

                                Row(
                                    Modifier
                                        .fillMaxSize(),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Spacer(modifier = Modifier.width(10.dp))
                                    Text(
                                        text = "$ 5.53",
                                        color = mycolor25,
                                        fontSize = 12.sp,
                                        fontFamily = Sora,
                                        fontWeight = FontWeight.Normal,
                                    )
                                }
                            }
                        }
                    }
                }


                Icon(
                    painter = painterResource(id = R.drawable.vector),
                    contentDescription = null,
                    modifier = Modifier
                        .size(24.dp),
                    tint = mycolor35
                )
            }
            Spacer(modifier = Modifier
                .height(5.dp)
            )

            Row(Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
                ) {
                Button(
                    onClick = { if (count.value!=0)navController.navigate("lastPage") }, colors =
                    ButtonDefaults.buttonColors(
                        containerColor = if (count.value!=0) myColor1 else mycolor26
                    ),
                    shape = RoundedCornerShape(25.dp)
                    ,
                    modifier = Modifier
                        .width(315.dp)
                        .height(62.dp)

                ) {
                    Text(text = if (count.value!=0)"Order" else "You should order at least one item!",
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        color = if (count.value!=0) Color.White else Color.Black,
                        fontSize =if (count.value!=0) 16.sp else 12.sp)
                }

            }




        }
    }


}
