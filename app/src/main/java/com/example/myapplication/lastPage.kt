package com.example.myapplication

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.mycolor31
import com.example.myapplication.ui.theme.mycolor36
import com.example.myapplication.ui.theme.mycolor37
import com.example.myapplication.ui.theme.mycolor38
import kotlinx.coroutines.launch

@SuppressLint("CoroutineCreationDuringComposition")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun lastPage(navController: NavController) {

    val scope = rememberCoroutineScope()
    val scaffoldState = rememberBottomSheetScaffoldState()


    BottomSheetScaffold(
        scaffoldState = scaffoldState,
        sheetPeekHeight = 125.dp,
        sheetContainerColor = Color.White,


        sheetContent = {
            Column(
                Modifier
                    .padding( start = 30.dp, end = 30.dp, bottom = 30.dp)

            ) {
                Row(Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly
                   ) {
                    Text(
                        text = "10 minutes left",
                        color = mycolor36,
                        fontSize = 16.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceEvenly) {
                        Row {
                            Text(
                                text = "Delivery to",
                                color = mycolor37,
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "JI.Kpg Sutoyo",
                                color = mycolor36,
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.SemiBold
                            )


                        }

                    }
                Spacer(modifier = Modifier.height(15.dp))
                //s

                Row(Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly) {
                    Box(
                        Modifier
                            .height(4.dp)
                            .width(71.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(mycolor38)
                    )
                    Box(
                        Modifier
                            .height(4.dp)
                            .width(71.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(mycolor38)
                    )
                    Box(
                        Modifier
                            .height(4.dp)
                            .width(71.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(mycolor38)
                    )
                    Box(
                        Modifier
                            .height(4.dp)
                            .width(71.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(mycolor38)
                    )

                }
                Spacer(modifier = Modifier.height(15.dp))
                //s
                Box(
                    modifier = Modifier
                        .width(318.dp)
                        .padding(start = 5.dp)
                        .height(90.dp)
                        .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color.Transparent),
                    contentAlignment = Alignment.Center
                )
                {
                    Row(Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceEvenly,
                        verticalAlignment = Alignment.CenterVertically) {
                        Box(
                            modifier = Modifier
                                .size(62.dp)
                                .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                                .clip(RoundedCornerShape(10.dp))
                                .background(Color.Transparent),
                            contentAlignment = Alignment.Center
                        )
                        {
                            Icon(
                                painter = painterResource(id = R.drawable.motorcycle_fill0_wght400_grad0_opsz24),
                                contentDescription = null,
                                modifier = Modifier
                                    .size(32.dp),
                                tint = myColor1
                            )
                        }
                        Column {
                            Text(
                                text = "Delivery your order",
                                color = mycolor36,
                                fontSize = 14.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.SemiBold
                            )
                            Spacer(modifier = Modifier.height(10.dp))
                            Text(
                                text = "we delivery your goods to you in",
                                color = mycolor37,
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.Normal
                            )
                            Text(
                                text = "the shortes possible time",
                                color = mycolor37,
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.Normal
                            )



                        }



                    }

                }
                //start

                Spacer(modifier = Modifier.height(30.dp))

                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(start = 5.dp, end = 5.dp),
                    horizontalArrangement = Arrangement.SpaceBetween) {

                    Row {
                        Box(
                            Modifier
                                .size(54.dp)
                                .clip(
                                    RoundedCornerShape(14.dp)
                                )
                        )
                        {
                            androidx.compose.foundation.Image(
                                painter = painterResource(id = R.drawable.pro2),
                                contentDescription = null,
                                Modifier
                                    .size(54.dp),
                                contentScale = ContentScale.Crop,

                                )

                            }
                        Spacer(modifier = Modifier.width(10.dp))
                        Column {
                            Text(
                                text = "Johan Havan",
                                color = mycolor36,
                                fontSize = 14.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.SemiBold
                            )
                            Spacer(modifier = Modifier.height(7.dp))
                            Text(
                                text = "Personal Courier",
                                color = mycolor37,
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.Normal
                            )
                        }

                    }

                    //s
                    Box(
                        modifier = Modifier
                            .size(54.dp)
                            .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                            .clip(RoundedCornerShape(14.dp))
                            .background(Color.Transparent),
                        contentAlignment = Alignment.Center
                    )
                    {
                        Icon(
                            painter = painterResource(id = R.drawable.telpon),
                            contentDescription = null,
                            modifier = Modifier
                                .size(24.dp),
                            tint = mycolor37
                        )

                    }


                }




            }
        }
    ) {
        Box(
            Modifier
                .fillMaxSize()
                ){

            Image(painter = painterResource(id = R.drawable.map), contentScale = ContentScale.Crop, contentDescription =null,
                modifier = Modifier
                    .size(1081.dp,1020.dp))

            Row(Modifier
                .fillMaxWidth()
                .padding(start = 30.dp, end = 30.dp, top = 30.dp),
                horizontalArrangement = Arrangement.SpaceBetween

            ){
                Box(
                    modifier = Modifier
                        .size(44.dp)
                        .clip(RoundedCornerShape(14.dp))
                        .background(Color.White)
                        .clickable { navController.popBackStack()  },
                    contentAlignment = Alignment.Center
                ){
                    Icon(
                        painter = painterResource(id = R.drawable.arrow_left),
                        contentDescription = null,
                        modifier = Modifier
                            .size(24.dp),
                        tint = mycolor36
                    )

                }
                //2
                Box(
                    modifier = Modifier
                        .size(44.dp)
                        .clip(RoundedCornerShape(14.dp))
                        .background(Color.White),
                    contentAlignment = Alignment.Center
                ){
                    Icon(
                        painter = painterResource(id = R.drawable.gps),
                        contentDescription = null,
                        modifier = Modifier
                            .size(24.dp),
                        tint = mycolor36
                    )
                }

            }

        }

        scope.launch {
            scaffoldState.bottomSheetState.expand()
        }


    }

}