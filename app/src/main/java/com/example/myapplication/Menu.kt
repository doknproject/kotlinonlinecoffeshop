package com.example.myapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.myColor10
import com.example.myapplication.ui.theme.myColor11
import com.example.myapplication.ui.theme.myColor2
import com.example.myapplication.ui.theme.myColor3
import com.example.myapplication.ui.theme.myColor4
import com.example.myapplication.ui.theme.myColor5
import com.example.myapplication.ui.theme.myColor6
import com.example.myapplication.ui.theme.myColor7
import com.example.myapplication.ui.theme.myColor8
import com.example.myapplication.ui.theme.myColor9
import com.example.myapplication.ui.theme.mycolor12
import com.example.myapplication.ui.theme.mycolor13
import com.example.myapplication.ui.theme.mycolor16

@Composable
fun MyGrid(navController: NavController) {
    val cardsData: List<Map<String, Any>> = listOf(
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.53",
            "rate" to "6.8"
        ),
        mapOf(
            "image" to R.drawable.coffee2,
            "content" to "with Oat Milk",
            "price" to "$ 3.90",
            "rate" to "7.9"
        ),
        mapOf(
            "image" to R.drawable.coffee2,
            "content" to "with water",
            "price" to "$ 3.78",
            "rate" to "7.2"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with sugar",
            "price" to "$ 2.22",
            "rate" to "6.3"
        ),
        mapOf(
            "image" to R.drawable.coffepic2,
            "content" to "with Chocolate",
            "price" to "$ 4.11",
            "rate" to "8,1"
        ),
        mapOf(
            "image" to R.drawable.coffee2,
            "content" to "with cheese",
            "price" to "$ 7.65",
            "rate" to "9.1"
        )
    )

    MyScreen(cardsData = cardsData, navController = navController)
}

// this is going to be magic..
@Composable
fun  myCard(map: Map<String, Any>,navController: NavController, modifier: Modifier = Modifier) {
    Box(
        Modifier
            .fillMaxSize()
            .offset(y = (-10).dp), contentAlignment = Alignment.Center) {


        Card(
            modifier = Modifier
                .height(239.dp)
                .width(149.dp),
            // elevation = CardDefaults.cardElevation(10.dp),
            //border = BorderStroke(3.dp, Color.Gray),
            colors = CardDefaults.cardColors(Color.White)
        ) {
            Column {
                Box( contentAlignment = Alignment.Center, modifier = Modifier
                    .width(141.dp)
                    .height(132.dp)
                    .clip(RoundedCornerShape(16.dp))
                    .padding(start = 8.dp, top = 8.dp)

                ) {
                    Image(
                        painter = painterResource(id = map["image"] as Int),
                        contentDescription = null,
                        modifier = Modifier
                            .width(141.dp)
                            .height(132.dp)
                            .clip(RoundedCornerShape(16.dp)),
                        contentScale = ContentScale.Crop
                    )
                    /*
                    Box(Modifier
                        .width(141.dp)
                        .height(132.dp)
                        .padding(bottom =100.dp, end = 100.dp )
                        .clip(RoundedCornerShape(18.dp))
                        .background(mycolor39)

                    ) {

                    }
                    */


                    Row(
                        Modifier
                            .fillMaxWidth()
                            .padding(start = 7.dp, bottom = 95.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ){
                        Icon(painter = painterResource(id = R.drawable.star)
                            , contentDescription = null ,
                            tint = mycolor16,
                            modifier = Modifier
                                .size(10.dp)
                        )
                        Spacer(modifier = Modifier.width(3.dp))
                        Text(text = map["rate"] as String,
                            color = Color.White,
                            fontFamily = Sora,
                            fontSize = 10.sp,
                            fontWeight = FontWeight.SemiBold,


                        )


                    }


                }
                Text(text = "Cappocino",
                    color = Color.Black,
                    fontFamily = Sora,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier
                        .padding(start = 14.dp, top = 11.dp)

                )

                Text(text = map["content"] as String,
                    color = myColor10,
                    fontFamily = Sora,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Normal,
                    modifier = Modifier
                        .padding(start = 11.dp)

                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 14.dp, end = 5.dp, top = 10.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = map["price"] as String,
                        color = myColor11,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 18.sp,
                        modifier = Modifier
                        //  .padding(start = 14.dp, top = 12.dp)
                    )
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .size(32.dp)
                            //  .padding(start = 5.dp)
                            //.padding.dp)
                            .clip(RoundedCornerShape(10.dp))
                            //  .padding(start = (14.dp))
                            .background(myColor1)
                            .clickable { navController.navigate("detailPage") }

                    )
                    {
                        Icon(imageVector = Icons.Default.Add, contentDescription = null, tint = Color.White,
                            modifier = Modifier
                                .size(16.dp))

                    }

                }

            }


        }


    }

}
@Composable
fun MyScreen(cardsData: List<Map<String, Any>>,navController: NavController) {
    LazyVerticalGrid(columns = GridCells.Fixed(2),
        //verticalArrangement = Arrangement.spacedBy(16.dp),
        // fasele do onsor be sorat ofoghi ziad mishe
        //  horizontalArrangement = Arrangement.spacedBy(0.dp),
        contentPadding = PaddingValues(30.dp),

    ) {
        items(cardsData) { map ->
            myCard(map, navController = navController)
        }
    }
}
enum class coffeCategory(val value: String){
    Cappuccino(value = "Cappuccino"),
    Machiato(value = "Machiato"),
    Latte(value = "Latte"),
    Mocha(value = "Mocha"),
    Americano(value = "Americano"),
    FlatWhite(value = "FlatWhite"),
    Affogato(value = "Affogato"),

}
fun getAllCategory() : List<coffeCategory> {
    return listOf(
        coffeCategory.Cappuccino, coffeCategory.Machiato, coffeCategory.Latte,
        coffeCategory.Mocha, coffeCategory.Americano,coffeCategory.FlatWhite,
        coffeCategory.Affogato
    )
}



@Composable
fun Menu(navController: NavController) {
    val gradient45 = Brush.linearGradient(
        colors = listOf(Color(0xFF313131), Color(0xFF131313)),
        start = Offset(0f, Float.POSITIVE_INFINITY),
        end = Offset(Float.POSITIVE_INFINITY, 0f)
    )
    var textvalue by remember {
        mutableStateOf("")
    }
    // in here  i need to add a scaffold and my program will be in it
    Box(
        Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.9f)) {

        Column(Modifier.fillMaxSize()) {
            Box(
                Modifier
                    .fillMaxWidth()
                    .height(260.dp)


                    .background(gradient45)
            ) {

            }
            Box(
                Modifier
                    .fillMaxSize()
                    .background(myColor2)
            ) {

            }

        }
        Column {
            Row(
                modifier = Modifier
                    .padding(start = 30.dp, top = 30.dp, end = 30.dp, bottom = 30.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column {
                    Text(
                        text = "Location",
                        color = myColor3,
                        fontFamily = Sora,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.Normal

                    )
                    Row {
                        Text(
                            text = "Iran, Qazvin",
                            color = myColor4,
                            fontFamily = Sora,
                            fontSize = 14.sp,
                            fontWeight = FontWeight.SemiBold

                        )
                        Icon(
                            imageVector = Icons.Default.KeyboardArrowDown,
                            contentDescription = null,
                            tint = myColor4
                        )

                    }
                }
                Box(
                    Modifier
                        .size(44.dp)
                        .clip(shape = RoundedCornerShape(14.dp))
                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(
                            id = R.drawable.prof1
                        ),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        // kodom bakhshe pic
                        // alignment = Alignment.BottomStart
                    )
                }
            }
            Row(
                modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center
            ) {
                TextField(
                    value = textvalue, singleLine = true, maxLines = 1, onValueChange = { newtext ->
                        textvalue = newtext
                    },
                    colors = TextFieldDefaults.colors(
                        unfocusedIndicatorColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        focusedContainerColor = myColor8,
                        unfocusedContainerColor = myColor7,
                        focusedLabelColor = myColor6,
                        unfocusedLabelColor = myColor6,
                        focusedTextColor = Color.White,
                        unfocusedTextColor = Color.White


                    ), modifier = Modifier
                        .width(315.dp)
                        .height(52.dp),
                    label = {
                        Text(text = "Search coffe")
                    },
                    leadingIcon = {

                        Icon(
                            // if i want use from resource i can use image vector or anything else
                            //and then i just have to use .vectorResource
                            imageVector = ImageVector.vectorResource(id = R.drawable.search_normal),
                            contentDescription = null,
                            Modifier.size(20.dp),
                            tint = Color.White
                        )
                    },

                    trailingIcon = {


                        Box(
                            Modifier
                                .padding(end = 6.dp)
                                .size(44.dp)
                                .clip(RoundedCornerShape(12.dp))
                                .background(myColor5)
                                .clickable { }, contentAlignment = Alignment.Center
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.furnitur_icon),
                                contentDescription = null,
                                Modifier.size(20.dp),
                                tint = Color.White
                            )
                        }
                    },
                    shape = RoundedCornerShape(16.dp)


                )
            }
            // we use this line to undrestand the diffrence 2 element
            Spacer(Modifier.height(24.dp))
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Box(
                    Modifier
                        .width(315.dp)
                        .height(140.dp)
                        .clip(shape = RoundedCornerShape(16.dp))
                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(id = R.drawable.image3),
                        contentDescription = null,
                        Modifier
                            .width(315.dp)
                            .height(140.dp),
                        contentScale = ContentScale.Crop,

                        )

                    Box(
                        modifier = Modifier
                            .padding(start = 23.dp, top = 13.dp)
                            .width(70.dp)
                            .height(30.dp)
                            .clip(shape = RoundedCornerShape(8.dp))
                            .background(myColor9)
                            .padding(bottom = 3.dp),
                        Alignment.Center,

                    ) {
                        Text(
                            text = "Promo",
                            color = Color.White,
                            fontFamily = Sora,
                            fontWeight = FontWeight.SemiBold,
                        )
                    }
                  Box(
                      Modifier
                          .padding(start = 24.dp, top = 37.dp)
                          .fillMaxWidth()
                  ){
                      Column {
                          Box(){
                              Box(
                                  Modifier
                                      .align(Alignment.BottomStart)
                                      .background(color = Color.Black)
                                      .width(225.dp)
                                      .height(30.dp))
                              Text(
                                  text = "Buy one get",
                                  color = Color.White,
                                  fontFamily = Sora,
                                  maxLines = 1,
                                  fontWeight = FontWeight.SemiBold,
                                  fontSize = 32.sp,
                                  modifier = Modifier
                                      .offset(y = -3.5.dp)
                              )

                          }
                          Box(modifier = Modifier
                              .padding(bottom = 7.dp)){
                              Box(
                                  Modifier
                                      .align((Alignment.BottomStart))
                                      .background(color = Color.Black)
                                      .width(168.dp)
                                      .height(30.dp)
                                      .padding(bottom = 5.dp)
                              )
                              Text(
                                  text = "one FREE",
                                  color = Color.White,
                                  fontFamily = Sora,
                                  fontWeight = FontWeight.SemiBold,
                                  fontSize = 32.sp,
                                  modifier = Modifier
                                      .offset( y = -10.dp)
                              )
                          }
                      }
                  }
                }
            }
            val scrollState = rememberScrollState()
            Row(
                Modifier
                    .fillMaxWidth()
                    .horizontalScroll(scrollState)
                    .padding(start = 37.dp, top = 24.dp)
            ){
                var turn by remember {
                    mutableStateOf(0)
                }


                for((index,category )in getAllCategory().withIndex()) {
                    Box(contentAlignment = Alignment.Center, modifier = Modifier
                        .clip(shape = RoundedCornerShape(12.dp))
                        .clickable { turn = index }
                        .background(if (turn == index) mycolor12 else Color.White)
                        .height(38.dp)
                        .padding(start = 11.dp, end = 11.dp)

                       // .padding(start = 8.dp, )
                    ){

                        Text(
                            text = category.value,
                            fontFamily = Sora,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 14.sp,
                            color = (if(turn == index) Color.White else mycolor13)
                        )
                    }

                    Spacer(Modifier.padding(6.dp))

                }

            }
            //Spacer(Modifier.padding(50.dp))
            MyGrid(navController)
            Box {
            }
        }

    }
}
