package com.example.myapplication

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.myapplication.ui.theme.Sora
import com.example.myapplication.ui.theme.myColor1
import com.example.myapplication.ui.theme.mycolor14
import com.example.myapplication.ui.theme.mycolor19
import com.example.myapplication.ui.theme.mycolor20
import com.example.myapplication.ui.theme.mycolor22
import com.example.myapplication.ui.theme.mycolor25
import com.example.myapplication.ui.theme.mycolor26
import com.example.myapplication.ui.theme.mycolor27
import com.example.myapplication.ui.theme.mycolor28
import com.example.myapplication.ui.theme.mycolor29
import com.example.myapplication.ui.theme.mycolor30
import com.example.myapplication.ui.theme.mycolor31
import com.example.myapplication.ui.theme.mycolor32
import com.example.myapplication.ui.theme.mycolor33
import com.example.myapplication.ui.theme.mycolor34

@Composable
fun orderPage(navController: NavController,count :MutableState<Int>) {

    Box(
        Modifier
            .fillMaxSize()
            .background(mycolor22)
    ) {

        Column(Modifier.fillMaxSize()) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(30.dp),
                horizontalArrangement = Arrangement.SpaceBetween

            ) {
                Icon(painter = painterResource(id = R.drawable.arrow_left),
                    contentDescription = null,
                    tint = mycolor14,
                    modifier = Modifier
                        .size(24.dp)
                        .clickable { navController.popBackStack() }
                )

                Text(
                    text = "order",
                    color = mycolor25,
                    fontSize = 20.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    modifier = Modifier
                        .padding(end = 130.dp)
                )
            }
            Row(
                Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                //ss
                Box(
                    Modifier
                        .width(315.dp)
                        .height(51.dp)
                        .clip(RoundedCornerShape(14.dp))
                        .background(mycolor26)
                )
                {
                    Row(
                        Modifier
                            .fillMaxSize()
                            .padding(5.dp),
                        horizontalArrangement = Arrangement.SpaceEvenly
                    ) {

                        var controller by remember {
                            mutableStateOf(1)
                        }

                        Box(modifier = Modifier
                            .width(153.5.dp)
                            .height(40.dp)
                            .clickable { controller = 1 }
                            .clip(RoundedCornerShape(10.dp))
                            .background(if (controller == 1) myColor1 else Color.Transparent),
                            Alignment.Center

                        ) {
                            Row(
                                Modifier
                                    .fillMaxSize(),
                                horizontalArrangement = Arrangement.Center,
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = "deliver",
                                    color = if (controller == 1) Color.White else mycolor25,
                                    fontSize = 18.sp,
                                    fontFamily = Sora,
                                    fontWeight = FontWeight.SemiBold,
                                )
                            }


                        }

                        // box 2
                        Box(modifier = Modifier
                            .width(153.5.dp)
                            .height(40.dp)
                            .clickable { controller = 2 }
                            .clip(RoundedCornerShape(10.dp))
                            .background(if (controller == 2) myColor1 else Color.Transparent),
                            Alignment.Center


                        ) {

                            Row(
                                Modifier
                                    .fillMaxSize(),
                                horizontalArrangement = Arrangement.Center,
                                verticalAlignment = Alignment.CenterVertically
                            ) {

                                Text(
                                    text = "Pick up",
                                    color = if (controller == 2) Color.White else mycolor25,
                                    fontSize = 18.sp,
                                    fontFamily = Sora,
                                    fontWeight = FontWeight.SemiBold,
                                )
                            }
                        }
                    }
                }
            }
            Spacer(modifier = Modifier.height(10.dp))
            //start column
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp)
            ) {

                Text(

                    text = "Delivery Address",
                    fontSize = 16.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    color = mycolor25
                )
            }

            //start column
            Spacer(modifier = Modifier.height(10.dp))

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp)
            ) {

                Text(

                    text = "Jl.kpg sutoyo",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    color = mycolor25
                )

            }
            //start column


            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp)
            ) {

                Text(

                    text = "kpg.sutoyo No.620,Bilzen, Tanjungbalai.",
                    fontSize = 12.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    color = mycolor27
                )
            }

            Spacer(modifier = Modifier.height(20.dp))

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp),

            ) {
                Box(
                    modifier = Modifier
                        .width(130.dp)
                        .height(27.dp)
                        .clip(RoundedCornerShape(16.dp))
                        .background(Color.Transparent)
                        .border(1.dp, mycolor28, RoundedCornerShape(12.dp)),
                    //contentAlignment = Alignment.Center
                )
                {

                    Row(
                        Modifier.fillMaxSize(),
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically
                    )
                    {
                        Icon(
                            painter = painterResource(id = R.drawable.edit),
                            contentDescription = null,
                            tint = mycolor29,
                            modifier = Modifier
                                .size(18.dp)
                                .padding(top = 4.dp)
                        )
                        Text(

                            text = "Edit Address",
                            fontSize = 12.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.Normal,
                            color = mycolor29
                        )
                    }
                }


                    //box 2
                Spacer(modifier = Modifier.width(13.dp))

                    Box(
                        modifier = Modifier
                            .width(110.dp)
                            .height(27.dp)
                            .clip(RoundedCornerShape(16.dp))
                            .background(Color.Transparent)
                            .border(1.dp, mycolor28, RoundedCornerShape(12.dp)),
                        //contentAlignment = Alignment.Center
                    )
                    {

                        Row(
                            Modifier.fillMaxSize(),
                            horizontalArrangement = Arrangement.Center,
                            verticalAlignment = Alignment.CenterVertically
                        )
                        {
                            Icon(
                                painter = painterResource(id = R.drawable.note),
                                contentDescription = null,
                                tint = mycolor29,
                                modifier = Modifier
                                    .size(18.dp)
                                    .padding(top = 4.dp)
                            )
                            Text(

                                text = "Add Note",
                                fontSize = 12.sp,
                                fontFamily = Sora,
                                fontWeight = FontWeight.Normal,
                                color = mycolor29

                            )
                        }

                    }
            }
            Spacer(modifier = Modifier.height(20.dp))
            // start column
            Divider(
                color = mycolor19,
                thickness = 1.dp,
                modifier = Modifier
                    .width(350.dp)
                    .padding(start = 30.dp)
            )
            Spacer(modifier = Modifier.height(20.dp))

            //start column

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp)) {

                Box(
                    Modifier
                        .size(54.dp)
                        .clip(
                            RoundedCornerShape(12.dp)
                        )
                )
                {
                    androidx.compose.foundation.Image(
                        painter = painterResource(id = R.drawable.coffepic2),
                        contentDescription = null,
                        Modifier
                            .size(54.dp),
                        contentScale = ContentScale.Crop,

                        )
                }
                Spacer(modifier = Modifier.width(13.dp))
                // start column

                Column {

                    Text(
                        text = "Cappucino",
                        fontSize = 16.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        color = mycolor30
                    )

                    Text(
                        text = "with Chocolate",
                        fontSize = 12.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        color = mycolor20
                    )

                }
                Spacer(modifier = Modifier.width(23.dp))

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 13.dp),
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically) {


                        Box(
                            modifier = Modifier
                                .size(28.dp)
                                .clip(RoundedCornerShape(20.dp))
                                .background(Color.Transparent)
                                .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                                .clickable { if (count.value > 0) --count.value },
                            contentAlignment = Alignment.Center
                        ) {

                            Icon(
                                painter = painterResource(id = R.drawable.minus),
                                contentDescription = null,
                                tint = if(count.value==0) mycolor32 else mycolor30,
                                modifier = Modifier
                                    .size(18.dp)
                            )

                        }
                        Spacer(modifier = Modifier.width(8.dp))

                        Text(
                            text = "${count.value}",
                            fontSize = 14.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.SemiBold,
                            color = mycolor30
                        )
                        //box 2
                        Spacer(modifier = Modifier.width(8.dp))

                        Box(
                            modifier = Modifier
                                .size(28.dp)
                                .clip(RoundedCornerShape(20.dp))
                                .background(Color.Transparent)
                                .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                                .clickable { count.value++ },
                            contentAlignment = Alignment.Center
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.plus),
                                contentDescription = null,
                                tint = mycolor30,
                                modifier = Modifier
                                    .size(18.dp)
                            )
                        }
                }

            }
            Spacer(modifier = Modifier.height(20.dp))

            Divider(
                color = mycolor33,
                thickness = 4.dp,
                modifier = Modifier
                    .fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(20.dp))

            Box(
                modifier = Modifier
                    .width(355.dp)
                    .height(56.dp)
                    .padding(start = 33.dp)
                    .border(1.dp, mycolor31, RoundedCornerShape(20.dp))
                    .clip(RoundedCornerShape(10.dp))
                    .background(Color.Transparent),
                contentAlignment = Alignment.Center
            )
            {
                Row(Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    verticalAlignment = Alignment.CenterVertically
                    ) {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .width(200.dp)
                            .height(350.dp)
                            .padding(end = 5.dp)
                    ){
                        Icon(
                            painter = painterResource(id = R.drawable.discount),
                            contentDescription = null,
                            modifier = Modifier
                                .size(20.dp),
                            tint = myColor1
                        )
                        Text(
                            text = "1Discount is applied",
                            fontSize = 14.sp,
                            fontFamily = Sora,
                            fontWeight = FontWeight.SemiBold,
                            color = mycolor30,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .padding( bottom = 5.dp, start = 6.5.dp)
                        )

                    }
                    Spacer(modifier = Modifier.width(15.dp))


                    Icon(
                        painter = painterResource(id = R.drawable.arrow___right_2),
                        contentDescription = null,
                        modifier = Modifier
                            .size(20.dp)
                            .clickable { },
                        tint = mycolor30
                    )
                }
            }

            Spacer(modifier = Modifier.height(8.dp))


            //start column
            Text(

                text = "Payment Summary",
                fontSize = 16.sp,
                fontFamily = Sora,
                fontWeight = FontWeight.SemiBold,
                color = mycolor30,
                modifier = Modifier
                    .padding(start = 33.dp)
            )

            //s
            Spacer(modifier = Modifier.height(20.dp))

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp, end = 33.dp ),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,


                ) {

                Text(

                    text = "Price",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    color = mycolor30,
                    modifier = Modifier
                )

                Text(

                    text = "$ 4.53",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    color = mycolor30,
                    modifier = Modifier

                )

            }
            Spacer(modifier = Modifier.height(20.dp))


            //start
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp, end = 33.dp ),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,


                ) {

                Text(

                    text = "Delivery Free",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    color = mycolor30,
                    modifier = Modifier
                )
                Row(

                ){

                    Text(

                        text = "$ 2.0",
                        fontSize = 14.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.Normal,
                        color = mycolor30,
                        textDecoration = TextDecoration.LineThrough

                    )
                    Spacer(modifier = Modifier.width(10.dp))

                    Text(

                        text = "$ 1.0",
                        fontSize = 14.sp,
                        fontFamily = Sora,
                        fontWeight = FontWeight.SemiBold,
                        color = mycolor30,
                        modifier = Modifier

                    )


                }
            }
                //s
            Divider(
                color = mycolor19,
                thickness = 1.dp,
                modifier = Modifier
                    .width(350.dp)
                    .padding(start = 30.dp)
            )

            Spacer(modifier = Modifier.height(20.dp))

            //s
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 33.dp, end = 33.dp ),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,


                ) {

                Text(

                    text = "Total Payment",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.Normal,
                    color = mycolor34,
                    modifier = Modifier
                )

                Text(

                    text = "$ 5.53",
                    fontSize = 14.sp,
                    fontFamily = Sora,
                    fontWeight = FontWeight.SemiBold,
                    color = mycolor34,
                    modifier = Modifier

                )

            }


            }
        //start

        //start column
        }

    }



